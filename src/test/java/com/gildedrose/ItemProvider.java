package com.gildedrose;

public enum ItemProvider {
    AGED_BRIE_NORMAL("Aged Brie", 10, 10, "Aged Brie", 9, 11),
    AGED_BRIE_DOUBLE_QUALITY_INCREASE("Aged Brie", -1, 10, "Aged Brie", -2, 12),
    AGED_BRIE_WITH_MAX_QUALITY("Aged Brie", 5, 50, "Aged Brie", 4, 50),
    AGED_BRIE_DOES_NOT_EXCEED_MAX_QUALITY("Aged Brie", -1, 49, "Aged Brie", -2, 50),
    ELIXIER_NORMAL_QUALITY_DECREASE("Elixir of the Mongoose", 5, 7, "Elixir of the Mongoose", 4, 6),
    ELIXIER_DOUBLE_QUALITY_DECREASE("Elixir of the Mongoose", 0, 7, "Elixir of the Mongoose", -1, 5),

    BACKSTAGE_PASS_QUALITY_INCREASE_BY_ONE("Backstage passes to a TAFKAL80ETC concert", 15, 20, "Backstage passes to a TAFKAL80ETC concert", 14, 21),
    BACKSTAGE_PASS_QUALITY_INCREASE_BY_TWO("Backstage passes to a TAFKAL80ETC concert", 10, 20, "Backstage passes to a TAFKAL80ETC concert", 9, 22),
    BACKSTAGE_PASS_DOES_NOT_EXCEED_MAX_QUALITY_WHEN_INCREASED_BY_TWO("Backstage passes to a TAFKAL80ETC concert", 10, 49, "Backstage passes to a TAFKAL80ETC concert", 9, 50),
    BACKSTAGE_PASS_QUALITY_INCREASE_BY_THREE("Backstage passes to a TAFKAL80ETC concert", 5, 20, "Backstage passes to a TAFKAL80ETC concert", 4, 23),
    BACKSTAGE_PASS_DOES_NOT_EXCEED_MAX_QUALITY_WHEN_INCREASED_BY_THREE("Backstage passes to a TAFKAL80ETC concert", 5, 49, "Backstage passes to a TAFKAL80ETC concert", 4, 50),
    BACKSTAGE_PASS_SET_QUALITY_TO_ZERO("Backstage passes to a TAFKAL80ETC concert", 0, 20, "Backstage passes to a TAFKAL80ETC concert", -1, 0),
    LEGENDARY_ITEM("Sulfuras, Hand of Ragnaros", -1, 80, "Sulfuras, Hand of Ragnaros", -1, 80);
    private String itemName;
    private int sellIn;
    private int quality;
    private String expectedName;
    private int expectedSellIn;
    private int expectedQuality;

    ItemProvider(String name, int sellIn, int quality, String expectedName, int expectedSellIn, int expectedQuality) {
        this.itemName = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.expectedName = expectedName;
        this.expectedSellIn = expectedSellIn;
        this.expectedQuality = expectedQuality;
    }

    public String getItemName() {
        return itemName;
    }

    public int getSellIn() {
        return sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public String getExpectedName() {
        return expectedName;
    }

    public int getExpectedQuality() {
        return expectedQuality;
    }

    public int getExpectedSellIn() {
        return expectedSellIn;
    }
}
